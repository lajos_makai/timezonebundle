<?php
/*
* This file is part of the MakaiTimezoneBundle package.
*
* (c) Makai Lajos
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Makai\TimezoneBundle\Helper;

use Symfony\Component\HttpFoundation\Session\Session;

/**
 * IP alapján GeoIP információkat tudhatunk meg.
 */
class IpDecoder
{
    /**
     * @var array
     */
    protected $ips = array('127.0.0.1' => null);

    /**
     * Szerver timeout. Ennél kevesebb idő alatt kell megoldani a lekérdezést!
     * @var int
     */
    protected $server_timeout;

    /**
     * Session-be mentjük el az aktuális adatokat, hogy userenként csak egyszer terheljük
     * lekéréssel az Ip2Geocode adatbázist. Ez lassú válasz esetén pláne fontos lehet.
     * @var \Symfony\Component\HttpFoundation\Session\Session
     */
    protected $session;

    protected $error_notifier;

    /**
     * @param int $server_timeout
     */
    public function __construct($server_timeout, Session $session, $error_notifier = null)
    {
        $this->server_timeout   = $server_timeout;
        $this->session          = $session;
        // Session-ből visszatöltjük a már kiolvasott adatokat.
        $this->ips              = array_merge($this->ips, $this->session->get($this->getSessionCacheName(), []));
        $this->error_notifier   = $error_notifier;
    }

    /**
     * A megadott IP-nek lekéri a GeoIP adatait.
     *
     * @param  string       $ip
     * @return [null|array]
     */
    public function find($ip)
    {
        // Cache-elünk, ha már lekértük egyszer az adatot, nem kérdezzük le megint.
        if(!array_key_exists($ip, $this->ips)) {
            // Beállítjuk, hogy max 5 másodpercig várjunk csak
            $ctx=stream_context_create(array('http'=>
                array(
                    'timeout' => min(5, $this->server_timeout/2)
                )
            ));

            //$ip_data = @file_get_contents('http://freegeoip.net/json/'.$ip, false, $ctx);
            $ip_data = @file_get_contents('http://www.telize.com/geoip/'.$ip, false, $ctx);

            if ($ip_data) {
                $datas = json_decode($ip_data, true);

                // Ha vmi probléma lép fel az adatok értelmezése közben, ugyanis volt ilyen hiba!
                if (!is_array($datas)) {
                    $datas = [];
                    if (preg_match('|country_code":("\w\w")|', $ip_data, $matches)) {
                        $datas['country_code'] = json_decode($matches[1]);
                    }
                    if (preg_match('|timezone":("[^"]+")|', $ip_data, $matches)) {
                        $datas['timezone'] = json_decode($matches[1]);
                    }

                    // A hibáról értesítést küldünk magunknak. Mivel csak a prod environmentben nincs definiálva, ezért csak magunknál exceptiont dobunk
                    $e = new \Exception('Invalid JSON string!');
                    if ($this->error_notifier) {
                        /*$this->error_notifier->createMailAndSend(
                            $e,
                            $this->getRequest()
                        );*/
                    } else {
                        throw $e;
                    }
                }

                // Ha a ```country_code``` ```RD``` értékkel rendelkezik, akkor ismeretlen IP-vel van dolga, ez rendszerint belső IP-t jelent: 127.0.0.1, 192.168.*.*, stb.
                $this->addIpData($ip, (!array_key_exists('country_code', $datas) || $datas['country_code'] == 'RD') ? null : $datas);
            } else {
                $this->addIpData($ip, null);
                $last_error = error_get_last() ?: [
                    'type'      => 0,
                    'message'   => '-',
                    'file'      => '-',
                    'line'      => 0,
                ];
                throw new \Exception(sprintf('Empty response! Last error (code %d): %s on line %d in %s', $last_error['type'], $last_error['message'], $last_error['line'], $last_error['file']));
            }
        }

        return $this->ips[$ip];
    }

    protected function getSessionCacheName()
    {
        return sprintf('%s_session_cache', get_class($this));
    }

    protected function addIpData($ip, $data)
    {
        $this->ips[$ip] = $data;
        $this->session->set($this->getSessionCacheName(), $this->ips);
    }

    /**
     * Lekérdezzük, hogy az adott IP melyik országhoz tartozik. Megadhatunk
     * ```$default``` értéket, amivel visszatér, ha nincs adat.
     *
     * @param type $ip
     * @param type $default
     * @return type
     */
    public function getCountry($ip, $default = null)
    {
        $datas = $this->find($ip);

        return is_null($datas) ? $default : $datas['country_code'];
    }

    public function getTimezone($ip, $default = null)
    {
        $datas = $this->find($ip);

        return is_null($datas) || !array_key_exists('timezone', $datas) ? $default : $datas['timezone'];
    }
}