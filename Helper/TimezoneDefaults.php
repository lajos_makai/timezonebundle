<?php
/*
* This file is part of the MakaiTimezoneBundle package.
*
* (c) Makai Lajos
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Makai\TimezoneBundle\Helper;

/**
 * Service: ```europestream.timezone.defaults```
 * A ```config.yml```-ben pl:
 * <code>
 *  parameters:
 *      europestream.timezone_defaults:
 *          'Europe/':
 *              currency:               EUR
 *              default_hourly_price:   10
 *          'America/':
 *              currency:               USD
 *              default_hourly_price:   10
 *          'Europe/Budapest':
 *              currency:               HUF
 *              default_hourly_price:   3000
 *          'Europe/London':
 *              currency:               GBP
 *              default_hourly_price:   10
 * </code>
 *
 * Ezzel a szolgáltatással tudjuk lekérdezni, hogy az adott időzónához milyen
 * alapértelmezett adatok (pl pénznem) vannak beállítva.
 */
class TimezoneDefaults
{
    protected $config;

    public function __construct($config)
    {
        $this->config = $config;
        uksort($this->config, array($this, 'sortByKeyLength'));
    }

    /**
     * String hosszúság szerint rendezzük csökkenő sorrendbe a beállításokat.
     * Erre azért van szükség, mert megadhatók ilyenek is: "Europe" vagy "America",
     * tehát nem konkrét időzóna, hanem egy nagyobb csoport.
     *
     * @param type $a
     * @param type $b
     * @return int
     */
    public function sortByKeyLength($a, $b)
    {
        $a = strlen($a);
        $b = strlen($b);

        if($a == $b) {
            return 0;
        }

        return ($a < $b) ? 1 : -1;
    }

    public function getConfig($timezone)
    {
        // Először megnézzük, hogy az adott időzónára van-e konkrét egyezés
        if(array_key_exists($timezone, $this->config)) {
            return $this->config[$timezone];
        }
        // Másodszor megnézzük, hogy van-e országra vonatkozó adat
        $tz = new \DateTimeZone($timezone);
        $location = $tz->getLocation();
        $country_code = '#'.$location['country_code'];
        if(array_key_exists($country_code, $this->config)) {
            return $this->config[$country_code];
        }
        // Végül azt ellenőrizzük, hogy van-e részegyezés pl: 'Europe/'
        foreach($this->config as $pattern => $config) {
            if(strpos($timezone, $pattern)===0) {
                return $config;
            }
        }

        return array();
    }
}