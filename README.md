## Telepítés ##

### For Symfony >= 2.3.* ###

A bundle dependecy-be tartozik a SonataIntlBundle:
Először ezt kell feltelepíteni:
```
http://sonata-project.org/bundles/intl/master/doc/index.html
```

Fel kell venni a composer.json fájlba:

```
{
    "require": {
        "makai/timezone-bundle": "dev-master",
    }
     "repositories": [
        {
            "type": "vcs",
            "url": "https://lajos_makai@bitbucket.org/lajos_makai/timezonebundle.git"
        }
    ]
}
```


### Bundle telepítése ###

```
$ php composer.phar update makai/timezone-bundle
```

### Bundle regisztrálása###

```php
// app/AppKernel.php

public function registerBundles()
{
    $bundles = array(
        new Makai\TimezoneBundle\MakaiTimezoneBundle(),
    );
}
```

app/routing.yml fájlba fel kell venni:

```
# MakaiTimezoneBundle
makai_timezone:
    resource: "@MakaiTimezoneBundle/Resources/config/routing.yml"
    prefix:   /
```

Assetek telepítése:
```
php app/console assets:install 
```
app/config/config.yml fájlba fel kell venni:
```
# Doctrine Configuration
doctrine:
    dbal:
        options:
            time_zone:  UTC
            collation:  utf8_unicode_ci
        types:
            datetime: Makai\TimezoneBundle\DBAL\Types\UTCDateTimeType


# SonataIntlBundle Configuration
sonata_intl:
    timezone:
        # default timezone used as fallback
        default: %sonata_intl_timezone_default%
	#override sonata_intl timezone detector
        service: makai.timezone.detector
```
app/config/parameters.yml fájlba fel kell venni:
```
# default timezone used as fallback
sonata_intl_timezone_default: Europe/Budapest
```
base.html.twig-be kell illeszteni:
```
        {#időzóna detector fájl#}
        <script src="{{ asset('web/bundles/makaitimezone/js/jstz.min.js') }}" type="text/javascript"></script>

        {#csak addig kell minden oldalbetöltéskor vizsgálni, hogy mi a felhasználó időzónája, amíg be nem lép az oldalra
        ha már belépett, akkor az adatbázisban lévő időzónát használjuk
        #}
        {% if app.user is null and not is_granted("IS_AUTHENTICATED_FULLY") %}
            <script type="text/javascript">
                jQuery(document).ready(function($) {
                    setTimeZone();
                });
            </script>
        {% endif %}
```
```
/*felhasználó időzónájának beállítása*/
function setTimeZone(){
    var timezone = jstz.determine();

    $.ajax({
        url: Routing.generate('makai_timezone_set_timezone'),
        type: "POST",
        dataType: "json",
        cache: false,
        data: {
            timezone: timezone.name()
        },
        success: function(data) {
        }
    });
}
```