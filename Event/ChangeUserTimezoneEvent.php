<?php
/*
* This file is part of the MakaiTimezoneBundle package.
*
* (c) Makai Lajos
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Makai\TimezoneBundle\Event;

use Symfony\Component\EventDispatcher\Event,
    Symfony\Component\HttpFoundation\Request;

class ChangeUserTimezoneEvent extends Event
{
    const TIMEZONE_CHANGE = 'change.user.timezone';
    
    protected $timezone;
    
    protected $request;
    
    public function __construct($timezone, Request $request) {

        $this->timezone = $timezone;
        $this->request  = $request;
    }
    
    public function setTimezone($timezone) {
        $this->timezone = $timezone;
        
        return $this;
    }
    
    public function getTimezone() {
        return $this->timezone;
    }
    
    public function setRequest(Request $request) {
        $this->request = $request;
        
        return $this;
    }
    
    public function getRequest() {
        return $this->request;
    }
}