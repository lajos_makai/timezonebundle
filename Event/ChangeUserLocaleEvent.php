<?php
/*
* This file is part of the MakaiTimezoneBundle package.
*
* (c) Makai Lajos
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Makai\TimezoneBundle\Event;

use Symfony\Component\EventDispatcher\Event,
    Symfony\Component\HttpFoundation\Request;

class ChangeUserLocaleEvent extends Event
{
    const LOCALE_CHANGE = 'change.user.locale';
    
    protected $locale;
    
    protected $request;
    
    public function __construct($locale, Request $request) {
        $this->locale = $locale;
        $this->request  = $request;
    }
    
    public function setLocale($locale) {
        $this->locale = $locale;
        
        return $this;
    }
    
    public function getLocale() {
        return $this->locale;
    }
    
    public function setRequest(Request $request) {
        $this->request = $request;
        
        return $this;
    }
    
    public function getRequest() {
        return $this->request;
    }
}