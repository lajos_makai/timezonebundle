<?php
/*
* This file is part of the MakaiTimezoneBundle package.
*
* (c) Makai Lajos
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Makai\TimezoneBundle\EventListener;

use Sonata\IntlBundle\Timezone\TimezoneDetectorInterface;

class TimezoneGetter implements TimezoneDetectorInterface
{

    protected $session;
    protected $container;

    public function __construct($session, $container)
    {
        $this->session = $session;
        $this->container = $container;
    }

    public function getTimezone(){
        if(!$this->session->has("_es_timezone")){
            if($this->container->get("request")->cookies->has('_es_timezone')){
                return $this->container->get("request")->cookies->get("_es_timezone");
            }
        }else{
            return $this->session->get("_es_timezone");
        }
        
        return "Europe/Paris";

    }
}