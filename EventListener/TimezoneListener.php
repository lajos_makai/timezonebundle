<?php
/*
* This file is part of the MakaiTimezoneBundle package.
*
* (c) Makai Lajos
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Makai\TimezoneBundle\EventListener;

use Symfony\Component\HttpFoundation\Session\Session,
    Symfony\Component\Security\Http\Event\InteractiveLoginEvent,
    Symfony\Component\HttpKernel\EventListener\LocaleListener,
    Symfony\Component\HttpKernel\Event\GetResponseEvent,
    Symfony\Component\HttpFoundation\Request,
    Doctrine\DBAL\Event\ConnectionEventArgs,
    Makai\TimezoneBundle\Event\ChangeUserTimezoneEvent,
    Makai\TimezoneBundle\Event\ChangeUserLocaleEvent;

class TimezoneListener extends LocaleListener
{
    protected $session;

    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    /**
     * Adatbázishoz kapcsolódás után beállítja a rendszer, hogy a MySQL megfelelő
     * értékeket adjon vissza a TIMESTAMP mezőknél!
     * @param \Doctrine\DBAL\Event\ConnectionEventArgs $args
     */
    public function postConnect(ConnectionEventArgs $args)
    {
        /**
         * 1. Beállítjuk, hogy az adatbázis milyen rendezés ÉS időzóna szerint működjön
         * ----------------------------------------------------------------------------
         */
        $connection_params  = $args->getConnection()->getParams();

        $charset            = $connection_params['charset'];
        $collation_value    = (array_key_exists('collation', $connection_params['driverOptions'])) ? $connection_params['driverOptions']['collation'] : null;
        $timezone           = $this->getTimezone() ?: ((array_key_exists('time_zone', $connection_params['driverOptions'])) ? $connection_params['driverOptions']['time_zone'] : null);

        $collation = ($collation_value) ? " COLLATE ".$collation_value : "";
        if($collation) {
            $args->getConnection()->executeUpdate("SET NAMES ".$charset . $collation);
        }

        if($timezone) {
            $args->getConnection()->executeUpdate("SET time_zone = :timezone", array('timezone' => $timezone));
        }
    }

    /**
     * Belépés után a felhasználó álltal beállított időzóna szerint módosítja az adatokat!
     * @param \Symfony\Component\Security\Http\Event\InteractiveLoginEvent $event
     */
    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        $user = $event->getAuthenticationToken()->getUser();
        /*ide inkább ilyen kellene
         * if($user instanceof User) {
         * */
        if($user) {
            $timezone = $user->getTimezone();
            if ($timezone) {
                $this
                    ->setTimezone($timezone)
                    ->setSystemTimezone($timezone);
            }

            $locale = $user->getLocale();
            if($locale) {
                $this->setLocaleToUser($event->getRequest(), $locale);
            }
        } else {
            throw new \Exception('Nem megfelelő User típus!');
        }
    }

    /**
     * Beállítja a rendszerben az időzónát, már ha van beállítva és mentve!
     * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        $this->setSystemTimezone($this->getTimezone());

        if($this->session->get('_locale')) {
            $this->setLocaleToUser($event->getRequest(), $this->session->get('_locale'));
        }
    }

    /**
     * Elmenti a megadott időzónát a session-be.
     * @param string $timezone
     */
    public function setTimezone($timezone)
    {
        $this->session->set('_es_timezone', $timezone);
        return $this;
    }

    /**
     * Visszaadja az elmentett időzónát!
     * @return string
     */
    public function getTimezone()
    {
        return $this->session->get('_es_timezone');
    }

    /**
     * Beállítja a megfelelő időzóna adatot a PHP-nak.
     * @param string $timezone
     */
    public function setSystemTimezone($timezone)
    {
        if($timezone) {
            date_default_timezone_set($timezone);
        }
        return $this;
    }

    /**
     * Beállítjuk a locale értéket.
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param type $locale
     */
    public function setLocaleToUser(Request $request, $locale)
    {
        $request->attributes->set('_locale', $locale);
        $this->session->set('_locale', $locale);
    }

    /**
     * Frissítjük az adatokat, ha változott az aktuális user időzónája. Controllerből kell hívni egyelőre!
     * <code>
     *   $this->get('event_dispatcher')->dispatch(
     *       ChangeUserTimezoneEvent::TIMEZONE_CHANGE,
     *       new ChangeUserTimezoneEvent($form->get('timezone')->getData(), $this->get('request')));
     * </code>
     */
    public function onChangeUserTimezone(ChangeUserTimezoneEvent $event) {
        $this
            ->setTimezone($event->getTimezone())
            ->setSystemTimezone($event->getTimezone());
    }

    /**
     * Frissítjük az adatokat, ha változott az aktuális user locale-je. Controllerből kell hívni egyelőre!
     * <code>
     *   $this->get('event_dispatcher')->dispatch(
     *       ChangeUserLocaleEvent::LOCALE_CHANGE,
     *       new ChangeUserLocaleEvent($form->get('locale')->getData(), $this->get('request')));
     * </code>
     */
    public function onChangeUserLocale(ChangeUserLocaleEvent $event) {
        $this->setLocaleToUser($event->getRequest(), $event->getLocale());
    }
}