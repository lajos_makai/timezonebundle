<?php
/*
* This file is part of the MakaiTimezoneBundle package.
*
* (c) Makai Lajos
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Makai\TimezoneBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller,
    Symfony\Component\HttpFoundation\JsonResponse,
    Symfony\Component\HttpFoundation\Request,
    Sensio\Bundle\FrameworkExtraBundle\Configuration\Template,
    Sensio\Bundle\FrameworkExtraBundle\Configuration\Route,
    Makai\TimezoneBundle\Event\ChangeUserTimezoneEvent;

class TimezoneController extends Controller
{

    public function setTimezoneAction() {

        $request = $this->getRequest();

        $timezone = $request->request->get('timezone', false);

        if ( ! $timezone) {
            throw new \Exception("Timezone is missing.");
        }

        if ( ! in_array($timezone, \DateTimeZone::listIdentifiers())) {
            return new Response("invalid timezone");
        }

        $this->get('event_dispatcher')->dispatch(
            ChangeUserTimezoneEvent::TIMEZONE_CHANGE,
            new ChangeUserTimezoneEvent($timezone, $request));

        return new JsonResponse(array("success"=>true));
    }
}
?>